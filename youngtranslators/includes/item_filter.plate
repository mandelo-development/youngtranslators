{% assign form_field_label_class = "--icon-input" %}
{% assign form_field_class = "form-field__input" %}
{% assign max_tarief = nil %}
{% assign min_tarief = nil %}
{% assign max_hours = nil %}
{% assign min_hours = nil %}
{% assign wordList = nil %}
{% assign dateList = nil %}

{% for words in site.content_objects.trefwoorden %}
    {% assign wordList = wordList | push: words.titel | uniq %}
{% endfor %}
{% for item in site.vacancies %}
    {% assign max_tarief = max_tarief | push: item.wage_to | uniq %}
    {% assign min_tarief = min_tarief | push: item.wage_from | uniq %}
    {% assign max_hours = max_hours | push: item.hours | uniq %}
    {% assign wordList = wordList | push: item.title | uniq %}
{% endfor %}
{% assign wordList = wordList | sort %}
{% assign locationList = site.content_objects.locatie | sort: 'titel' %}

<div class="search contact-form {% unless title_form == true %}plate--container{% endunless %} form-default-search {% if title_form == true %}title-search{% endif %}">
    <div class="form-group">
        <div class="form-group__fields fl-container jc-between fl-wrap">
            <div class="{% if title_form != true %}plate--row{% else %}fl-container fl-row{% endif %} form-group__fields--wrapper">
                <div class="plate--column form-group__fields_column_1 {% unless title_form == true %}md-4{% endunless %}">
                    {% assign form_field_label = "Functie, trefwoord" %}
                    <div class="form-field-container {{form_field_label_class}}">
                        <div class="form-field">
                            <label class="form-field__label">{{form_field_label}}</label>
                            {{ 'trefwoord' | form_input_name | html_input: 'text', id: 'trefwoorden', value: '', class: form_field_class, list: 'trefwoorden' }}
                            <datalist id="trefwoorden">
                                {% for word in wordList %}
                                    <option value="{{word}}">
                                {% endfor %}
                            </datalist>
                            {% include 'includes/icons/work' %}
                        </div>
                        {% assign x = form_field.name %}
                    </div>
                </div>
                <div class="plate--column form-group__fields_column_2 {% unless title_form == true %}md-5{% endunless %} place-search">
                    {% assign form_field_label = "Zoek op plaatsnaam..." %}
                    <div class="fl-container fl-row form-field-container--mixed --text">
                        <div class="form-field-container {{form_field_label_class}}">
                            <div class="form-field">
                                <label class="form-field__label">{{form_field_label}}</label>
                                {{ 'regio' | form_input_name | html_input: 'text', id: 'locations', value: '', class: form_field_class, list: 'regio' }}
                                <datalist id="regio">
                                    {% for item in locationList %}
                                        <option value="{{item.titel}}">
                                    {% endfor %}
                                </datalist>
                                {% include 'includes/icons/location' %}
                            </div>
                        </div>
                        <div class="form-field-container radius">
                            <div class="form-field select fl-container fl-row ai-center">
                                <select name="radius-filter" id="radius__filter__input" data-object="{{post.content_type.name}}" data-parent="radius_input" class="form-field__select">
                                    <option value="all">Alle afstanden</option>
                                    <option selected value="10000">+10 km</option>
                                    <option value="20000">+20 km</option>
                                    <option value="30000">+30 km</option>
                                    <option value="40000">+40 km</option>
                                    <option value="50000">+50 km</option>
                                    <option value="60000">+60 km</option>
                                    <option value="70000">+70 km</option>
                                    <option value="80000">+80 km</option>
                                    <option value="90000">+90 km</option>
                                    <option value="100000">+100 km</option>
                                    <option value="150000">+150 km</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plate--column form-group__fields_column_3 {% unless title_form == true %}md-2{% endunless %} filter-search">
                    <div class="fl-container fl-row ai-center jc-between">
                        {% unless title_form == true %}<p class="--nomargin collapse-search">Uitgebreid zoeken</p>{% endunless %}
                        {% assign svg = '<svg xmlns="http://www.w3.org/2000/svg" width="16.711" height="16.711" viewBox="0 0 16.711 16.711"><g id="download_50_" data-name="download (50)" transform="translate(-0.983 -0.983)"><path id="Path_336" data-name="Path 336" d="M8.623.983a7.639,7.639,0,1,0,4.629,13.7L15.979,17.4A1.006,1.006,0,1,0,17.4,15.979L14.68,13.252A7.623,7.623,0,0,0,8.623.983ZM8.623,3A5.621,5.621,0,1,1,3,8.623,5.606,5.606,0,0,1,8.623,3Z" fill="#4e469e"/></g></svg>' %}
                        {% include 'elements/buttons/button', background: 'var(--blue)', text_color: 'var(--dark-blue)', search: true, link: site.vacancies_index.url, text: svg %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{% assign min_tarief = min_tarief | sort %}
{% assign max_tarief = max_tarief | sort %}
{% assign max_hours = max_hours | sort %}
{% unless title_form == true %}
<div class="search contact-form plate--container form-advanced-search">
    <div class="form-group">
        <div class="form-group__fields fl-container jc-between fl-wrap">
            <div class="plate--row form-group__fields--wrapper">
                <div class="plate--column md-3 hour-rate">
                    {% assign form_field_label = "Uurtarief" %}
                    <div class="fl-container fl-row form-field-container--mixed --numbers --label-active">
                        <div class="form-field-container">
                            <div class="form-field">
                                <label class="form-field__label">{{form_field_label}}</label>
                                <div class="fl-container fl-row range-container"> 
                                    {{ 'min' | form_input_name | html_input: 'number', value: min_tarief.first, id: 'hour-min', class: 'form-field__input --min', min: min_tarief.first, max: max_tarief.last }}
                                    {{ 'max' | form_input_name | html_input: 'number', value: max_tarief.last, id: 'hour-max', class: 'form-field__input --max', min: min_tarief.first, max: max_tarief.last }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plate--column md-3 week-rate">
                    {% assign form_field_label = "Startdatum" %}
                    <div class="fl-container fl-row form-field-container --date --label-active">
                        <div class="form-field-container">
                            <div class="form-field">
                                <label class="form-field__label">{{form_field_label}}</label>
                                <div class="fl-container fl-row range-container"> 
                                    {% assign nowDate = 'now' | date: "%s"%}
                                    {{ 'date' | form_input_name | html_input: 'date', value: nowDate, id: 'startdate', class: 'form-field__input --min', min: max_hours.first, max: max_hours.last }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plate--column md-3 week-rate">
                    {% assign form_field_label = "Uren per week" %}
                    <div class="fl-container fl-row form-field-container--mixed --numbers --label-active">
                        <div class="form-field-container">
                            <div class="form-field">
                                <label class="form-field__label">{{form_field_label}}</label>
                                <div class="fl-container fl-row range-container"> 
                                    {{ 'min' | form_input_name | html_input: 'number', value: max_hours.first, id: 'week-min', class: 'form-field__input --min', min: max_hours.first, max: max_hours.last }}
                                    {{ 'max' | form_input_name | html_input: 'number', value: max_hours.last, id: 'week-max', class: 'form-field__input --max', min: max_hours.first, max: max_hours.last }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="plate--column {% unless title_form == true %}md-2{% endunless %} filter-search">
                    <div class="fl-container fl-row ai-center jc-between search-reset-wrapper">
                        <p class="--nomargin search-reset">Reset</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% endunless %}
