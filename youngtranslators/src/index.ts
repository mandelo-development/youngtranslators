import '../../config/node_modules/regenerator-runtime/runtime';
import './scripts/scrollContainer';
import './scripts/functions';
import './scripts/lazyload';
import './scripts/updatecss';
import { vacancySlider, reviewSlider } from './scripts/swiper';
vacancySlider();
reviewSlider();

import './styles/style';


