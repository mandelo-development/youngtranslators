import gsap from '../../../config/node_modules/gsap';
import { setCookie } from './functions';

export const landingPageTL = gsap.timeline();
export const landingPageParent = document.querySelector('.load-container');

if(landingPageParent) {
    var landingPageLogo = landingPageParent.querySelector('svg');
}

function landingPage() {
    
    landingPageTL.to(landingPageLogo, {
        scale: 0,
        duration: 0
    }, "+=1" ).to(landingPageLogo, {
        scale: 1.2,
        duration: .3
        // duration: .8,
        // y: -Math.abs(window.innerHeight),
        // autoAlpha: 0,
    }, "-=0.1").to(landingPageLogo, {
        scale: .8,
        duration: .3
        // duration: .8,
        // y: -Math.abs(window.innerHeight),
        // autoAlpha: 0,
    }, "-=0.1").to(landingPageLogo, {
        scale: 1,
        duration: .6
        // duration: .8,
        // y: -Math.abs(window.innerHeight),
        // autoAlpha: 0,
    }, "-=0.1").to(landingPageLogo, {
        scale: 0,
        duration: .3
        // duration: .8,
        // y: -Math.abs(window.innerHeight),
        // autoAlpha: 0,
    }, "-=0.1").to(landingPageParent, {
        duration: .8,
        y: -Math.abs(window.innerHeight + 200),
    }, "+=0.7").to(landingPageParent, {
        autoAlpha: 0,
    }, "+=0.1");
    

    // landingPageTL.to(landingPageParent, {
    //     onComplete: () => contentAnimationIn(),
    // }, "-=0.6");
    
    // console.log('set cookie');
    // setCookie('introAnimation', true, 365, '/' );
}

export{
    landingPage
} 
