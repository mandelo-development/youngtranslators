
import {scrollCont, barba} from './scrollContainer';
import {debounce, throttle, setActive} from './functions';
import gsap from '../../../config/node_modules/gsap';
import '../../../config/node_modules/bootstrap/js/dist/collapse';

var $ = require("../../../config/node_modules/jquery/dist/jquery");

async function ifEditing() {
    $(document).ready( function () {
        if(parent.document.getElementById('config-bar')) {
            $('body').addClass('config-mode');
            barba.destroy();
            setTimeout(() => {
                scrollCont.destroy();
            }, 100);
            
        }
    });
}
async function formLabels() {
[].forEach.call(
    document.querySelectorAll('.form-field__label, .form-field__input, .form-field__textarea'),
    el => {
        el.onblur = () => {
            setActive(el, false);
            
        };
        el.onfocus = () => {
            setActive(el, true);
        };
    });
}


async function openActiveTab() {
    [].forEach.call(
        document.querySelectorAll('.tab--button'),
        el => {
            el.addEventListener('click', () => {
                openTab(el.dataset.id, el)
            })
        }
    )

    function openTab(tab, elem) {
        var q;
        var y = document.getElementsByClassName("tab--button");
        for (q = 0; q < y.length; q++) {
            y[q].classList.remove('--active');
        } 
        
        elem.classList.add('--active');

        var i;
        var x = document.getElementsByClassName("tab-item");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
            x[i].classList.remove('--active');
        }

        document.getElementById(tab).style.display = "block";
        document.getElementById(tab).classList.add('--active');
    }
}
   

async function Accordion() {
    var mediaQuery = window.matchMedia('(min-width: 990px)');
    if (mediaQuery.matches) {
        $('.accordion').mouseenter(function(){
            scrollCont.stop();
        }).mouseleave(function(){
            scrollCont.start();
            scrollCont.update();
        });
    }
    $('.card').on('click', function(){
      
        if(!$(this).hasClass('active') ){  
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
        } else {
            $(this).removeClass('active'); 
        }    
        scrollCont.update();
    });
}
  

async function jobAlerts() {
    $(window).click(function() {
        $('.main-alerts').removeClass('--active');
    });
      
    $('.main-alerts').on('click', function(e) {
        e.stopPropagation();
        $('.main-alerts').addClass('--active');
    });
}

async function numberRange() {
    [].forEach.call(
        document.querySelectorAll('input[type="number"]'),
        el => {
            $(el).on('keyup', debounce(function(){

                var v = parseInt($(this).val());
                var min = parseInt($(this).attr('min'));
                var max = parseInt($(this).attr('max'));
                console.log(v)
                if (isNaN(v)) {

                    if ($(this).hasClass('--max')){
                        v = max + 1;
                    } else {
                        v = min - 1;
                    }
                }

                
                if (v < min || v == null ) {
                    $(this).val(min);
                } else if (v > max) {
                    $(this).val(max);
                }
            }, 500));
        }
    )
}


async function updatePadding() { 
   
    $(document).ready( function () {
        if ($(window).width() <= 1024) {
            $(window).scroll(function () {
                console.log('testtt');
                if ($(window).scrollTop() >= 40) {
                    $('.navigation').addClass('fixed-header');
                } else {
                    $('.navigation').removeClass('fixed-header');
                }

            
            });
           

            
        } else {
            // Hide Navbar on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 1;
            var navbar = $('.navigation');
            var navbarHeight = $(navbar).outerHeight();

            scrollCont.on('scroll', function (event) {
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {

                var offsetScroll = $('#js-scroll').offset().top;
                var st = Math.abs(offsetScroll);

                // Make sure they scroll more than delta
                if(Math.abs(lastScrollTop - st) <= delta)
                    return;

                if (navbarHeight > st) {
                    $(navbar).addClass('nav-top');
                    $(navbar).removeClass('fixed-header');
                } else {
                    $(navbar).removeClass('nav-top');
                    setTimeout( function () { 
                        $(navbar).addClass('fixed-header');
                    }, 500)
                }

                // If they scrolled down and are past the navbar, add class .nav-down.
                // This is necessary so you never see what is "behind" the navbar.
                if (st > lastScrollTop && st > navbarHeight){
                    // Scroll Down
                    $(navbar).removeClass('nav-up').addClass('nav-down');
                } else {
                    // Scroll Up
                    if(st + $(window).height() < $(document).height()) {
                        $(navbar).removeClass('nav-down').addClass('nav-up');
                    }
                }
                
                lastScrollTop = st;
            }
        }
    });
}
async function copyLink() {
    $('.copy-link').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        copyToClipboard(href);
    });

    const copyToClipboard = (href) => {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(href).select();
        document.execCommand("copy");
        $temp.remove();
    }
}


async function autoFitText(){
    [].forEach.call(
        document.querySelectorAll('.--auto-size'),
        el => {
            var textLength = el.innerHTML;
           
            console.log();

            if (WordCount(textLength) > 35) {
                el.style.fontSize = '2rem';
            } else {
                el.style.fontSize = '3rem';
            }
        }
    );

    function WordCount(str) { 
        return str.split(" ").length;
    }
}

async function fileDrop() {
    [].forEach.call(
        document.querySelectorAll('.file-drop'),
        el => {
            var fileInput = el.parentNode.parentNode.parentNode.parentNode.querySelector('input');
            el.addEventListener("click", function(event){
                event.preventDefault();
                fileInput.click();    
            });

            fileInput.addEventListener("change", function(event){
                var fileInputText;
                if (fileInput.files.length > 1){
                    fileInputText = fileInput.files.length + ' bestanden geselecteerd'; 
                } else if (fileInput.files.length == 1) {    
                    fileInputText = fileInput.files.length + ' bestand geselecteerd'; 
                } else {
                    fileInputText = 'Plaats je bestanden hier'; 
                }

                el.querySelector('span').innerHTML = fileInputText;
            });
        }
    );
}

async function imageHeight() {
    [].forEach.call(
        document.querySelectorAll('.--auto-image'),
        el => {
            el.parentNode.classList.add('--auto')
        }
    );
}



export {
    ifEditing, formLabels, updatePadding, copyLink, jobAlerts, numberRange, fileDrop, autoFitText, openActiveTab, Accordion, imageHeight
}