import {scrollCont} from './scrollContainer';
import {debounce} from './functions';
import { masonryLayout } from './masonryLayout';
var $ = require("../../../config/node_modules/jquery");

async function filter(currentContainer) {
	var selectedFilters = [];
	var postType;
	
	var theEnd = false;
	var appended = false;
	var clickedLoadMore = false;
	var i = 0;
	
	var container = currentContainer.querySelector("#result__wrapper");
	var taxonmyFilters = currentContainer.querySelectorAll(".filter-button");
	var taxonomyFiltersSelect = currentContainer.querySelector("#taxonomy__select__input");
	var hourFiltersSelect = currentContainer.querySelector("#hour__select__input");
	var employmentFiltersSelect = currentContainer.querySelector("#employment__select__input");
	var loadMoreBtn = currentContainer.querySelector('.result .load-more');
	var eventListener = function () {
		var pageId = currentContainer.querySelector('.plate--page-content-wrapper').id;
		scrollCont.scrollTo('#' + pageId);
	}

	selectedFilters = selectedFilters;
	selectedFilters['page'] = 1;

	if ($(container).length > 0) {

		function makeRequest(data, type) {
			var text = [];
			var i;
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					text.push(i + "=" + encodeURIComponent(data[i]));
				}
			}
			var textState = [];
			var i;
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					if (i != 'page') {
						textState.push(i + "=" + decodeURIComponent(data[i]));
					}
				}
			}
			text = text.join("&");
			textState = textState.join("&");
			var url = "/filters?" + text;
			var pushStateUrl = "?" + textState;
			var loader = "<div class='loader'></div>";

			window.history.pushState(null, null, pushStateUrl);

			var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
			xhr.onreadystatechange = text;
			$(container).append(loader);
			xhr.open("GET", url, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(text);
			xhr.onload = function() {
				setTimeout(function() {
					container.classList.add("fade-out");
					if (type == 'append') {
						container.innerHTML = container.innerHTML + xhr.response;
					} else {
						container.innerHTML = xhr.response;
					}
					if (!xhr.response.includes('+==THEEND==+')) {
						setTimeout(function() {
							appended = false;
						}, 500);
					} else {
						theEnd = true;
					}
					
					setTimeout(function() {
						$('.loader').remove();
						container.classList.remove('fade-out');
						
						if(container.classList.contains('masonry')) {
							masonryLayout();
						}
						scrollCont.update();
					}, 50);
					scrollCont.update();
				}, 100);
			};
			xhr.onerror = function() {
				alert("error");
			};
		}

		function loadNextPage(scrollY) {
			let resultElement = document.querySelector('#result__wrapper');
			let position = elementInViewport(resultElement, resultElement.offsetTop, resultElement.offsetHeight, 100, scrollY);
			if (position && !appended && !theEnd) {
				appended = true;
				selectedFilters['page'] = selectedFilters['page'] + 1;
				makeRequest(selectedFilters, 'append');
			}
		}

		if (loadMoreBtn) {
			loadMoreBtn.addEventListener('click', function (clickEvent) {
				clickEvent.preventDefault();
				this.classList.add('hide-btn');
				loadNextPage(false);
				clickedLoadMore = true;
				newItemsOnScroll();
			});
		}

		function newItemsOnScroll() {
			if (currentContainer.classList.contains('paginate_index')) {
				const mediaQuery = window.matchMedia('(min-width: 1024px)')
				if (mediaQuery.matches) {
					scrollCont.on('scroll', function (scrollEvent) {
						var scrollYOffset;
							scrollYOffset = scrollEvent.scroll.y;
						loadNextPage(scrollYOffset);
					})
				} else {
					$(document).on('scroll', function (scrollEvent) {
						var scrollYOffset;
							scrollYOffset = window.pageYOffset;
						loadNextPage(scrollYOffset);
					})
				}
			}
		}
		

		if (taxonmyFilters !== null) {
			for (; i < taxonmyFilters.length; i++) {
				postType = taxonmyFilters[i].getAttribute("data-object");
				selectedFilters.data_object = postType;
				taxonmyFilters[i].addEventListener("click", debounce(function (e) {
					selectedFilters = selectedFilters;
					selectedFilters['page'] = 1;
					appended = false;
					theEnd = false;

					$(taxonmyFilters).parent().removeClass('active');
					$(this).parent().addClass('active'); 

					var parentTax = this.getAttribute("data-parent");
					var buttonId = this.getAttribute("value");
					selectedFilters[parentTax] = buttonId;

					makeRequest(selectedFilters);
				}, 100));
			}
		}

		if (taxonomyFiltersSelect !== null) {
			taxonomyFiltersSelect.addEventListener('change', debounce(function(event) {
				selectedFilters['page'] = 1;
				appended = false;
				theEnd = false;

				var parentTax = this.getAttribute("data-parent");
				var sortValue = this.options[this.selectedIndex].value;

				postType = this.getAttribute("data-object");
				selectedFilters.data_object = postType;
				selectedFilters[parentTax] = sortValue;

				makeRequest(selectedFilters);
			}, 500));
		}

		if (employmentFiltersSelect !== null) {
			employmentFiltersSelect.addEventListener('change', debounce(function(event) {
				selectedFilters = selectedFilters;
				selectedFilters['page'] = 1;
				appended = false;
				theEnd = false;

				var parentTax = this.getAttribute("data-parent");
				var sortValue = this.options[this.selectedIndex].value;

				postType = this.getAttribute("data-object");
				selectedFilters[parentTax] = sortValue;
				selectedFilters.data_object = postType;

				makeRequest(selectedFilters);
			}, 500));
		}

		if (hourFiltersSelect !== null) {
			hourFiltersSelect.addEventListener('change', debounce(function(event) {
				selectedFilters = selectedFilters;
				selectedFilters['page'] = 1;
				appended = false;
				theEnd = false;

				var parentTax = this.getAttribute("data-parent");
				var sortValue = this.options[this.selectedIndex].value;

				postType = this.getAttribute("data-object");
				selectedFilters[parentTax] = sortValue;
				selectedFilters.data_object = postType;

				makeRequest(selectedFilters);
			}, 500));
		}

		$(function() {
			var paramFilters = [];
			// Filter based on parameters in URL
			var param_object = getUrlParameter('data_object');
			var param_categories = getUrlParameter('categories');
			var param_hours = getUrlParameter('hours');
			var param_employments = getUrlParameter('employments');

			if (param_object || param_categories || param_hours || param_employments) {

				paramFilters.data_object = param_object;

				if (param_categories !== undefined) {
					var categoriesElement = $('*[value="' + param_categories + '"]');

					paramFilters.categories = param_categories;

					$(taxonmyFilters, taxonomyFiltersSelect).removeClass('active');
					$(categoriesElement).attr('selected','selected').addClass('active');

				}
				if (param_hours !== undefined) {
					var hoursElement = $('*[value="' + param_hours + '"]');

					paramFilters.hours = param_hours;

					$(hourFiltersSelect).removeClass('active');
					$(hoursElement).attr('selected','selected').addClass('active');

				}
				if (param_employments !== undefined) {
					var employmentsElement = $('*[value="' + param_employments + '"]');

					paramFilters.employments = param_employments;

					$(employmentFiltersSelect).removeClass('active');
					$(employmentsElement).attr('selected','selected').addClass('active');

				}

				makeRequest(paramFilters);
			}

		});

		// Get parameters from URL
		function getUrlParameter(sParam) {
			var sPageURL = window.location.search.substring(1),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
				} else {}
			}
		}
	}

	function elementInViewport(el, top, height, offset, scrollY) {

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
		}

		if (scrollY === false) {
			return true;
		} else {
			return (
				(scrollY + window.innerHeight) > (height + top - offset)
			);
		}
	}
} export {
	filter
}