const formSubmit = () => {

	var forms =  document.querySelectorAll(".contact-form form");
	if (typeof(forms) != 'undefined' && forms != null) {
		forms.forEach((form) => {
            var fileInputs = form.querySelectorAll('.form-field__file');
            if(fileInputs !== null) {
                Array.prototype.forEach.call( fileInputs, function( input ) {
                    var label	 = input.nextElementSibling,
                        labelVal = label.querySelector( 'span' ).innerHTML,
                        uploadIcon = label.querySelector('.file-icon'),
                        crossIcon = label.querySelector('.cross-icon');
                    input.addEventListener('change', function(e) {
                        console.log(e);
                        console.log(this.files, this.files.length)
                        var fileName = '';
                        if( this.files && this.files.length > 1 ) {
                            fileName = ( this.getAttribute( 'data_multiple_caption' ) || '' ).replace( '-count-', this.files.length );
                        } else {
                            fileName = e.target.value.split( '\\' ).pop();
                        }
                        console.log(fileName)
                        if( fileName ) {
                            label.querySelector( 'span' ).innerHTML = fileName;
                            console.log(uploadIcon, crossIcon)
                            uploadIcon.classList.add('hide-icon');
                            crossIcon.classList.add('show-icon');
                        } else {
                            label.querySelector( 'span' ).innerHTML = labelVal;
                            uploadIcon.classList.remove('hide-icon');
                            crossIcon.classList.remove('show-icon');
                        }
                    });
                    crossIcon.addEventListener('click', function(e) {
                        e.preventDefault();
                        if (e.target.classList.contains('cross-icon')) {
                            input.value = null;
                            label.querySelector( 'span' ).innerHTML = labelVal;
                            uploadIcon.classList.remove('hide-icon');
                            crossIcon.classList.remove('show-icon');
                        }
                    });
                });
            }

            form.addEventListener('submit', function(e) {
                // e.preventDefault()
                console.log(e)

                executeRecaptcha(form, function(){
                    form.submit();
                })
            });
        });
	}
}



export {
    formSubmit
}