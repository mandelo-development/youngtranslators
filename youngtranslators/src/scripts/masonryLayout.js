import * as Masonry from "../../../config/node_modules/masonry-layout";

async function masonryLayout() {

    var msnry = new Masonry( '.items-container.masonry', {
        percentPosition: true
    });

} export {
    masonryLayout
}