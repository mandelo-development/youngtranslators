var $ = require("../../../config/node_modules/jquery");
import { scrollCont } from './scrollContainer';

const slidingTabs = () => {
    $(document).ready(function () {
        var intervalId = window.setInterval(function () {
            tabs();
        }, 500);
    });

    window.addEventListener('resize', tabs);
    var tabContentWrapperTabs = $('.sliding-tabs__content__tabs');
    const minHeight = tabContentWrapperTabs[0].scrollHeight;
    const tabs = () => {
        var tabs = $('.sliding-tabs');

        if ($(tabs).length) {

            var tabListItems = $('.sliding-tabs .sliding-tabs__item');
            var tabContentParent = $('.sliding-tabs__content__text');


            var tabContent = $('.sliding-tabs .sliding-tabs__content__text__wrapper');
            var activeTabContent = $('.sliding-tabs .sliding-tabs__content__text__wrapper.active');



            $(tabContent).each(function () {
                $(this).css('height', $(this).outerHeight());
            });

            // addHeight(activeTabContent);

            $(activeTabContent).each(function (i) {
                // console.log($(this), $(this).data('element-id'))
                addHeight($(this), $(this).data('element-id'));
            });

            $(tabListItems).each(function (i) {

                $(this).on("click", function (e) {
                    e.preventDefault();
                    var elementID = $(this).data("id-element");
                    var activeListItem = $('.sliding-tabs[data-tabs-id="' + elementID + '"] .sliding-tabs__item.active');
                    activeTabContent = $('.sliding-tabs[data-tabs-id="' + elementID + '"] .sliding-tabs__content__text__wrapper.active')

                    var dataID = $(this).data("id");
                    var element = $('.sliding-tabs[data-tabs-id="' + elementID + '"] .sliding-tabs__content__text__wrapper[id="' + dataID + '"]');

                    $(activeListItem).removeClass('active');
                    $(activeTabContent).removeClass('active');
                    $(this).addClass('active');
                    $(element).addClass('active');

                    setTimeout(function () {
                        addHeight(element, elementID);

                    }, 100);

                });
            });

            function addHeight(activeItem, id) {
                console.log()
                // console.table('test', activeItem.find('.sliding-tabs__content__item').outerHeight(), {id}, tabContentParent[0])
                var heightActive = $(activeItem).find('.sliding-tabs__content__item').outerHeight() + 20;
                console.log(heightActive);

                if (heightActive < minHeight) {
                    heightActive = minHeight;
                }
                console.log({ heightActive }, { minHeight });
                if (id !== undefined) {
                    var parentTab = $('.sliding-tabs[data-tabs-id="' + id + '"]').find(tabContentParent);
                    // console.log(parentTab[0])


                    $(parentTab[0]).height(heightActive);
                } else {
                    $(tabContentParent).height(heightActive);
                }
                setTimeout(function () {
                    scrollCont.update();
                }, 400);
            }

        }
    }
}

export {
    slidingTabs
}