// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller,
	Mousewheel,
	Lazy,
	EffectFade,
	Autoplay,
} from '../../../config/node_modules/swiper/core';
import { gsap } from '../../../config/node_modules/gsap';
import { TextPlugin } from '../../../config/node_modules/gsap/TextPlugin';
import { SplitText } from '../../../config/node_modules/gsap/SplitText';

gsap.registerPlugin(TextPlugin);
gsap.registerPlugin(SplitText);

Swiper.use([Navigation, Pagination, Scrollbar, Controller, Mousewheel, Lazy, EffectFade, Autoplay]);

import { openActiveTab } from './general';
const pageSlider = () => {
	var pageSliders =  document.querySelectorAll('.page-slider__slider');
	if (typeof(pageSliders) != 'undefined' && pageSliders != null) {
		pageSliders.forEach((pageSlider) => {
			pageSlider = new Swiper ('.page-slider__slider', {
				slidesPerView: 1,
				spaceBetween: 0,
				direction: 'horizontal',
				loop: true,
				centeredSlides: true,
				autoplay: {
					delay: 5000,
					disableOnInteraction: false,
				},
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
				runCallbacksOnInit: true,
				on: {
					init: function () {
						typeText(this.$el);
					},
					slideNextTransitionStart: function () {
						typeText(this.$el);
					},
					slidePrevTransitionStart: function () {
						typeText(this.$el);
					} 
				}
			}); 
		});
	}
}

function typeText(sliderDOM){
	var slide = sliderDOM.find('.swiper-slide');
	var allSlideTitles = slide.find('h2');
	for (var i = 0; i < allSlideTitles.length; i++) {
		allSlideTitles[i].querySelector('span').innerHTML = '';
	}

	const slideActive = sliderDOM.find('.swiper-slide-active, .swiper-slide-duplicate-active');
  	const SlideTitel = slideActive.find('h2');
	for (var i = 0; i < SlideTitel.length; i++) {
		gsap.to(SlideTitel[i].querySelector('span'), {text: {value: SlideTitel[i].dataset.title}, duration: 1, delay: 0.2, ease: "Power3.ease"})
	}
}

const mediaSlider = () => {
	var mediaSliders =  document.querySelectorAll('.media-slider__slider.--slider');
	if (typeof(mediaSliders) != 'undefined' && mediaSliders != null) {
		mediaSliders.forEach((mediaSlider) => {
			mediaSlider = new Swiper ('.media-slider__slider', {
				slidesPerView: 1,
				spaceBetween: 30,
				direction: 'horizontal',
				loop: true,
				speed: 600,
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
				navigation: {
					nextEl: ".swiper-button-next.--header",
					prevEl: ".swiper-button-prev.--header",
				},	
			});
		});
	}
}

const vacancySlider = () => {
	var vacancySliders =  document.querySelectorAll('.vacancy-slider__slider');
	if (typeof(vacancySliders) != 'undefined' && vacancySliders != null) {
		vacancySliders.forEach((vacancySlider) => {
			vacancySlider = new Swiper ('.vacancy-slider__slider', {
				direction: 'horizontal',
				loop: false,
				speed: 600,
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
				breakpoints: {
					0: {
						slidesPerView: 1.1,
						spaceBetween: 30,
					},
					767: {
						slidesPerView: 2.5,
						spaceBetween: 30,
					},
					1400: {
						slidesPerView: 4,
						spaceBetween: 60,
					}
				},
			});
		});
	}
}

const serviceSlider = () => {
	var serviceSliders =  document.querySelectorAll('.service-slider');
	if (typeof(serviceSliders) != 'undefined' && serviceSliders != null) {
		serviceSliders.forEach((serviceSlider) => {
			serviceSlider = new Swiper ('.service-slider', {
				direction: 'horizontal',
				speed: 600,
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
				navigation: {
					nextEl: ".swiper-button-next.--services",
					prevEl: ".swiper-button-prev.--services",
				},	
				breakpoints: {
					0: {
						slidesPerView: 1.2,
						spaceBetween: 30,
					},
					767: {
						slidesPerView: 3,
						spaceBetween: 30,
					},
					1400: {
						slidesPerView: 4,
						spaceBetween: 30,
					}
				},
			});
			openActiveTab();
			
		});
		
	}
}

const logoSlider = () => {
	var logoSliders =  document.querySelectorAll('.logo-slider__logos');
	if (typeof(logoSliders) != 'undefined' && logoSliders != null) {
		logoSliders.forEach((logoSlider) => {
			var slides = logoSlider.querySelectorAll('.swiper-slide');
			var width = logoSlider.scrollWidth * 6;
			var widthMob = logoSlider.scrollWidth * 12;

			logoSlider = new Swiper ('.logo-slider__logos', {
				direction: 'horizontal',
				loop: true,
				speed: widthMob,
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
				centeredSlides: true,
				autoplay: {
					delay: 1,
					disableOnInteraction: false,
				},
				slidesPerView: "auto",
				breakpoints: {
					1200: {
						speed: width,
					}
				},
			});
		});
	}
}

const brancheSlider = () => {
	var brancheSliders =  document.querySelectorAll('.branche-slider__branches');
	if (typeof(brancheSliders) != 'undefined' && brancheSliders != null) {
		brancheSliders.forEach((brancheSlider) => {
			var slides = brancheSlider.querySelectorAll('.swiper-slide');
			brancheSlider = new Swiper ('.branche-slider__branches', {
				direction: 'horizontal',
				loop: true,
				slidesPerView: "auto",
				speed: 600,
				roundLengths: true,
				watchSlidesProgress: true,
				mousewheel: {
					forceToAxis: true,
				},
			});
		});
	}
}



const reviewSlider = () => {
	var reviewSliders =  document.querySelectorAll('.review-slider__slider');
	if (typeof(reviewSliders) != 'undefined' && reviewSliders != null) {
		reviewSliders.forEach((reviewSlider) => {
			reviewSlider = new Swiper ('.review-slider__slider', {
				autoHeight: true,
				direction: 'horizontal',
				loop: false,
				speed: 600,
				roundLengths: true,
				watchSlidesProgress: true,
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
				mousewheel: {
					forceToAxis: true,
				},
				breakpoints: {
					0: {
						slidesPerView: 1,
						spaceBetween: 0,
					}
				},
				pagination: {
					el: '.swiper-pagination',
					type: 'bullets',
					clickable: true
				},
			});
		});
	}
}

export {
	vacancySlider, reviewSlider, serviceSlider, pageSlider, mediaSlider, logoSlider, brancheSlider
}