async function playVideo(alreadyClicked) {
	let videos = document.querySelectorAll('.video-element:not(.not-loaded) .video-element__play');
    if (videos) {
		for (var i = 0; i < videos.length; i++) {
			let thisVideo = videos[i];
			if(alreadyClicked === true)	{
				let targetId = thisVideo.getAttribute('data-id');
				let targetSource = document.getElementById(targetId).getAttribute('video-src');

				document.getElementById(targetId).setAttribute('src', targetSource);
				document.getElementById(targetId).parentElement.classList.add('video--loaded');
			} else {
				thisVideo.addEventListener('click', function() {
					let targetId = this.getAttribute('data-id');
					let targetSource = document.getElementById(targetId).getAttribute('video-src');
					
					document.getElementById(targetId).setAttribute('src', targetSource);
					document.getElementById(targetId).parentElement.classList.add('video--loaded');
				})
			}
		}
	}
}
export {
	playVideo
}