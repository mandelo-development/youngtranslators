import barba from '../../../config/node_modules/@barba/core';
import LocomotiveScroll from '../../../config/node_modules/locomotive-scroll';
import { observeImages } from './lazyload';
import { landingPage } from './animation';
import { formLabels, ifEditing, updatePadding, jobAlerts, numberRange, fileDrop, autoFitText, Accordion, imageHeight } from './general';
import { filter } from './filter';
import { gsap } from '../../../config/node_modules/gsap';
import { ScrollTrigger } from "../../../config/node_modules/gsap/ScrollTrigger";
import { vacancySlider, reviewSlider, serviceSlider, pageSlider, mediaSlider, logoSlider, brancheSlider } from './swiper';
import { formSubmit } from './form';
import { slidingTabs } from './sliding_tabs';
import { descriptionUpdate } from './description';
import { menu } from './menu';
import { modal } from './modal';
import { autoPlay } from './autoPlay'
// gsap.registerPlugin(ScrollTrigger);

let scrollCont;
let scrollY = 0


barba.hooks.once((data) => {
    ifEditing();
    
    // landingPage();
    
    // pageTransitionOut("", true);
});
barba.hooks.leave((data) => {
  
    var closeModal = document.getElementById("video-popup");
    closeModal.querySelector("iframe").setAttribute('src', '');
    closeModal.classList.remove('is-visible');
    closeModal.style.display = "none";
});
barba.hooks.afterLeave((data) =>  {
  
    observeImages();
});
barba.hooks.beforeEnter((data) => {
    // ScrollTrigger.getAll().forEach(t => t.kill());
    // ScrollTrigger.removeEventListener("refresh", () => scrollCont.update());
    document.querySelector('body').classList.remove('lock-scroll');
    // customCursor(data.next.container);
    // Forms
    formLabels();
    numberRange();
    fileDrop();
    formSubmit();
    Accordion();
    autoFitText(); 
    jobAlerts();
    vacancySlider();
    reviewSlider();
    serviceSlider();
    logoSlider();
    pageSlider();
    mediaSlider();
    brancheSlider();
    slidingTabs();
    imageHeight();
    modal(); 
    descriptionUpdate();
    autoPlay(data.next.container);
});
barba.hooks.afterEnter((data) => {
    menu(data.next.container, data);
    updatePadding();
    // stickyScroll(gsap, ScrollTrigger, data.next.container);
})
barba.hooks.enter((data) => {
    
})
barba.hooks.after((data) => {   
    // scrollCont.scrollTo(0,0, {disableLerp: true});
    observeImages();
    scrollCont.update();
});

barba.init({
    debug: true, //verwijderen voor livegang
    timeout: 5000,
    views: [{
        namespace: 'home',
        beforeEnter(data) {
        },
        enter() {

        },
    },  {
        namespace: 'case-index',
        beforeEnter(data) {
            filter(data.next.container);
        },
        afterEnter(data) {
           
            if (scrollY != 0){
                if (data.next.url.path != data.current.url.path){
                    scrollCont.scrollTo(scrollY, 0, {disableLerp: true});
                }
            } scrollY = 0;
        },
        beforeLeave(data){
            
            let historyScrollPositon = scrollCont.el.style.transform.split(',');
            scrollY = Math.abs(parseInt(historyScrollPositon[13]));
        },
    }],
	transitions: [{
            name: 'page',
            sync: false,
            once({ next }) {
                // init LocomotiveScroll on page load
                smooth(next.container);
            },
            leave({ current }) {
                //const done = this.async();
                // pageTransitionIn(done);
                current.container.classList.add('leave');
                return gsap.to(current.container, {
                    opacity: 0,
                    ease: 'Power1.easeInOut',
                    duration: .2,
                });
            },
            beforeEnter({ next }) {
                // destroy the previous scroll
                scrollCont.destroy();

                // init LocomotiveScroll regarding the next page
                smooth(next.container);
                
            },
            enter({ next }) {
                next.container.classList.add('come');
                setTimeout(function () {
                    next.container.classList.remove('come')
                }, 200);
                return gsap.from(next.container, {
                    opacity: 0,
                    ease: 'Power1.easeInOut',
                    duration: .2,
                });
            }
        },
    ]
});

function smooth(container) {
    scrollCont = new LocomotiveScroll({
        el: container.querySelector('[data-scroll-container]'),
        smooth: true,
        useKeyboard: false,
        lerp: 0.1,
        scrollFromAnywhere: true,
        // reloadOnContextChange: true,
        resetNativeScroll: true,
        getDirection: true,
        tablet: {
            smooth: true,
        },
        smartphone: {
            smooth: false,
        },
    });

  
  

    // var circleSVG = container.querySelector(".community--link");
    // if (circleSVG) {
    //     scrollCont.on('scroll', (instance) => {
    //         scrollRotate(circleSVG, instance.scroll.y);
    //     });
    
    //     function scrollRotate(element, pageOffset) {
    //         gsap.to(element, {
    //             rotate: pageOffset / 20,
    //         })
    //         // element.style.transform = "rotate(" + pageOffset/6 + "deg)";
    //     }
    // }
}

export {
    scrollCont, barba, ScrollTrigger
};