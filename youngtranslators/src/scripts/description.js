import {scrollCont} from './scrollContainer';
import {debounce} from './functions';
import { autoFitText } from './general';
var $ = require("../../../config/node_modules/jquery/dist/jquery");

async function descriptionUpdate(currentContainer) {
    [].forEach.call(
        document.querySelectorAll('.description_list'),
        el => {
            var descriptionChildren = el.querySelectorAll('.description_list--item'),
                descriptionText = el.querySelector('.description_list-side-description');
                
            descriptionChildren.forEach((description) => {
                description.addEventListener('click', () => {
                    var prevActive = el.querySelector('.--active'),
                        desc = description.dataset.description;

                    prevActive.classList.remove('--active');
                    description.classList.add('--active');
                    descriptionText.innerHTML = desc;
                    
                    autoFitText();
                    scrollCont.update();
                });
            });   
        }
    )
} export { 
    descriptionUpdate
}