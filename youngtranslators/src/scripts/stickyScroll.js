import {scrollCont} from "./scrollContainer"
const stickyScroll = (gsap, ScrollTrigger, container) => {
  var circleSteps = container.querySelector(".steps-circle");
  const mediaQuery = window.matchMedia('(min-width: 1024px)')

  if (circleSteps) {


    ScrollTrigger.matchMedia({
      "(min-width: 1024px)": function() {
        scrollCont.on("scroll", ScrollTrigger.update);
        // tell ScrollTrigger to use these proxy methods for the "[data-scroll-container]" element since Locomotive Scroll is hijacking things
        ScrollTrigger.scrollerProxy("[data-scroll-container]", {
            scrollTop(value) {
              return arguments.length ? scrollCont.scrollTo(value, 0, 0) : scrollCont.scroll.instance.scroll.y;
            }, // we don't have to define a scrollLeft because we're only scrolling vertically.
            getBoundingClientRect() {
              return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
            },
            // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
            pinType: container.querySelector("[data-scroll-container]").style.transform ? "transform" : "fixed"
        });

      },
    
    }); 
    
    var steps = document.querySelectorAll('.steps-circle__item');
    var stepsTriangle = Array.from(document.querySelectorAll(".steps-circle__item .icon-bg"));
    var stepsText = document.querySelectorAll('.steps-circle__text__wrapper');
    var tl = gsap.timeline({paused: true});

    function onReverse(func, onlyAfterComplete, param1) {
      let time = 0,
        reversed;
      return function() {
        let t = this.time(),
          r = t < time;
        r && !reversed && (!onlyAfterComplete || time === this.duration()) && func.call(this, param1);
        time = t;
        reversed = r;
      };
    }

    stepsTriangle.forEach((stepTriangle, i) => {

      tl.to(stepTriangle, {
        fill: '#FBC02D',
        duration: 1,
        stagger: 0.4,
        onStart: fadeInText,
        onStartParams: [i, stepTriangle],

        onComplete: fadeOutText,
        onCompleteParams: [i, stepTriangle],
        
        onUpdate: onReverse(fadeInText, true, i),
        
        onReverseComplete: fadeOutText,
        onReverseCompleteParams: [i, stepTriangle],
        ease: "Power3.ease",
        
      }).add("step" + i);
    });

    function fadeInText(i, element) {
      var stepText = document.querySelector('.steps-circle__text__wrapper.__' + i);

      gsap.to(stepText, {
          autoAlpha: 1,
          duration: 1,
          ease: "Power3.ease",
      });
    }
    function fadeOutText(i, element) {
      var stepText = document.querySelector('.steps-circle__text__wrapper.__' + i);

      gsap.to(stepText, {
        autoAlpha: 0,
        duration: 1,
        ease: "Power3.ease",
      });
    }
    

    ScrollTrigger.matchMedia({
      "(min-width: 1024px)": function() {
        scrollCont.on("scroll", ScrollTrigger.update);
        // tell ScrollTrigger to use these proxy methods for the "[data-scroll-container]" element since Locomotive Scroll is hijacking things
        ScrollTrigger.scrollerProxy("[data-scroll-container]", {
            scrollTop(value) {
              return arguments.length ? scrollCont.scrollTo(value, 0, 0) : scrollCont.scroll.instance.scroll.y;
            }, // we don't have to define a scrollLeft because we're only scrolling vertically.
            getBoundingClientRect() {
              return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
            },
            // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
            pinType: container.querySelector("[data-scroll-container]").style.transform ? "transform" : "fixed"
        });

      },
    
    }); 


    ScrollTrigger.matchMedia({
      "(min-width: 1024px)": function() {
        scrollCont.on("scroll", ScrollTrigger.update);
        // tell ScrollTrigger to use these proxy methods for the "[data-scroll-container]" element since Locomotive Scroll is hijacking things
        ScrollTrigger.scrollerProxy("[data-scroll-container]", {
            scrollTop(value) {
              return arguments.length ? scrollCont.scrollTo(value, 0, 0) : scrollCont.scroll.instance.scroll.y;
            }, // we don't have to define a scrollLeft because we're only scrolling vertically.
            getBoundingClientRect() {
              return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
            },
            // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
            pinType: container.querySelector("[data-scroll-container]").style.transform ? "transform" : "fixed"
        });

      },
    
    });
    
    ScrollTrigger.matchMedia({
      "(min-width: 1024px)": function() {

        ScrollTrigger.create({
          animation: tl,
          trigger: ".steps-circle__circle",
          scroller: "[data-scroll-container]",
          scrub: true,
          pin: true,
          anticipatePin: 1,
          // pinReparent: true,
          start: "top top",
          end: "+=200%",
        })

        ScrollTrigger.addEventListener("refresh", () => scrollCont.update());
        ScrollTrigger.refresh(true);

      },
      "(max-width: 1023px)": function() {
        ScrollTrigger.create({
          animation: tl,
          trigger: ".steps-circle",
          scrub: true,
          pin: true,
          anticipatePin: 1,
          // pinReparent: true,
          start: "top top",
          end: "+=100%",
        })
      },
    }); 
  }
}
export {
  stickyScroll
}