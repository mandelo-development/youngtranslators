async function autoPlay(container) {
	var videoTriggers = container.querySelectorAll('[restartautoplay]');
    if (typeof(videoTriggers) != 'undefined' && videoTriggers != null) {
		for (var i = 0; i < videoTriggers.length; i++) {
			let thisVideo = videoTriggers[i];
            thisVideo.play();
		}
	}
}
export {
	autoPlay
}