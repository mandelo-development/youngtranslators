


import { throttle } from './functions';
import gsap from '../../../config/node_modules/gsap';
import { scrollCont } from './scrollContainer';
var $ = require("../../../config/node_modules/jquery/dist/jquery");

async function menu(currentContainer, currentContainerData) {
    var clicks = !0,
        navbarToggler = currentContainer.querySelector(".navbar-toggler");
    // communityLink = currentContainer.querySelector(".community--link");

    if (navbarToggler) {

        function runMenu(e) {
            !0 === e ? menuTL.timeScale(1).play() : menuTL.timeScale(2.5).reverse()
        }

        navbarToggler.addEventListener("click", throttle(function(e) {
            var t = currentContainer.querySelector(".navigation"),
                o = document.querySelector("body");
            (clicks = !clicks) ? (o.classList.remove("lock-scroll"), runMenu(!1), t.classList.remove("menu-open")) : (o.classList.add("lock-scroll"), runMenu(!0), t.classList.add("menu-open"))
        }, 1000));
    }  
    
    var alinks = currentContainer.querySelectorAll('[data-scroll-to]');
	if (typeof(alinks) != 'undefined' && alinks != null) {
		alinks.forEach((link) => { 
            link.addEventListener("click", function(e) {
                e.preventDefault();
                var target = '#' + link.hash.substr(1);  
                scrollCont.scrollTo(target);
            });
        })
    }
    
    var hreflinks = currentContainer.querySelectorAll('#' + currentContainerData.next.url.hash);

	if (typeof(hreflinks) != 'undefined' && hreflinks != null) {
		hreflinks.forEach((link) => { 
            scrollCont.scrollTo(link);
            scrollCont.update();
        })
    }
}
export {
    menu
}